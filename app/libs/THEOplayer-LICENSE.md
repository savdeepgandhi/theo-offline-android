# THEOplayer Android license

Please copy received THEOplayer license file
`theoplayer-android-[name]-[version]-minapi16-release.aar`
into this folder and rename it to `theoplayer.aar`.

See [THEOplayer SDK Integration] for more information about adding
THEOplayer Android license.

Please visit [Get Started with THEOplayer] to get license
for Android applications.


[//]: # (Links reference)
[Get Started with THEOplayer]: https://www.theoplayer.com/licensing

