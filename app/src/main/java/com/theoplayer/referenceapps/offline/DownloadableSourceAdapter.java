package com.theoplayer.referenceapps.offline;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

import java.io.InputStream;
import java.util.List;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

class DownloadableSourceAdapter extends RecyclerView.Adapter<DownloadableSourceAdapter.ViewHolder> {

    private Context context;

    private List<DownloadableSource> downloadableSources;

    private Consumer<DownloadableSource> onStartDownloadActionHandler;
    private Consumer<DownloadableSource> onPauseDownloadActionHandler;
    private Consumer<DownloadableSource> onRemoveDownloadActionHandler;
    private Consumer<DownloadableSource> onPlayActionHandler;


    DownloadableSourceAdapter(Context context, List<DownloadableSource> downloadableSources) {
        this.context = context;
        this.downloadableSources = downloadableSources;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.downloadable_source, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return downloadableSources.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        DownloadableSource source = downloadableSources.get(position);

        // View tag is used to determine if requested poster already loaded and shown.
        // If so, there's no need to reload it.
        if (!source.getPoster().equals(viewHolder.posterImageView.getTag())) {
            try (InputStream posterInputStream = context.getAssets().open(source.getPoster())) {
                Drawable posterDrawable = Drawable.createFromStream(posterInputStream, null);
                viewHolder.posterImageView.setImageDrawable(posterDrawable);
                viewHolder.posterImageView.setTag(source.getPoster());
            } catch (Exception ignore) {
                // Displaying poster placeholder in case of any problems with loading requested poster.
                viewHolder.posterImageView.setImageResource(R.mipmap.ic_launcher);
            }
        }

        viewHolder.titleTextView.setText(source.getTitle());

        viewHolder.startButton.setEnabled(source.isStateUpToDate());
        viewHolder.pauseButton.setEnabled(source.isStateUpToDate());
        viewHolder.removeButton.setEnabled(source.isStateUpToDate());

        if (source.isDownloadable()) {
            viewHolder.startButton.setVisibility(source.isDownloading() || source.isDownloaded() || source.isFailed() ? GONE : VISIBLE);

            viewHolder.pauseButton.setVisibility(source.isDownloading() ? VISIBLE : GONE);

            viewHolder.removeButton.setVisibility(VISIBLE);

            viewHolder.progressBar.setVisibility(VISIBLE);
            viewHolder.progressBar.setProgress(source.getProgress());

            viewHolder.progressTextView.setVisibility(VISIBLE);
            viewHolder.progressTextView.setText(context.getString(R.string.progressLabel, source.getProgress()));
        } else {
            viewHolder.startButton.setVisibility(VISIBLE);
            viewHolder.pauseButton.setVisibility(GONE);
            viewHolder.removeButton.setVisibility(GONE);
            viewHolder.progressBar.setVisibility(GONE);
            viewHolder.progressTextView.setVisibility(GONE);
        }

        viewHolder.container.setStrokeColor(
                source.isFailed() ? context.getResources().getColor(R.color.theoError) : 0
        );
    }

    void setOnStartDownloadActionHandler(Consumer<DownloadableSource> onStartDownloadActionHandler) {
        this.onStartDownloadActionHandler = onStartDownloadActionHandler;
    }

    void setOnPauseDownloadActionHandler(Consumer<DownloadableSource> onPauseDownloadActionHandler) {
        this.onPauseDownloadActionHandler = onPauseDownloadActionHandler;
    }

    void setOnRemoveDownloadActionHandler(Consumer<DownloadableSource> onRemoveDownloadActionHandler) {
        this.onRemoveDownloadActionHandler = onRemoveDownloadActionHandler;
    }

    void setOnPlayActionHandler(Consumer<DownloadableSource> onPlayActionHandler) {
        this.onPlayActionHandler = onPlayActionHandler;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        MaterialCardView container;
        MaterialButton startButton;
        MaterialButton pauseButton;
        MaterialButton removeButton;
        TextView titleTextView;
        ImageView posterImageView;
        ProgressBar progressBar;
        TextView progressTextView;

        ViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            titleTextView = itemView.findViewById(R.id.titleTextView);
            posterImageView = itemView.findViewById(R.id.posterImageView);
            startButton = itemView.findViewById(R.id.startButton);
            pauseButton = itemView.findViewById(R.id.pauseButton);
            removeButton = itemView.findViewById(R.id.removeButton);
            progressBar = itemView.findViewById(R.id.progressBar);
            progressTextView = itemView.findViewById(R.id.progressTextView);

            container.setOnClickListener(onClickListener(onPlayActionHandler));
            startButton.setOnClickListener(onClickListener(onStartDownloadActionHandler));
            pauseButton.setOnClickListener(onClickListener(onPauseDownloadActionHandler));
            removeButton.setOnClickListener(onClickListener(onRemoveDownloadActionHandler));
        }

        private View.OnClickListener onClickListener(Consumer<DownloadableSource> actionHandler) {
            return view -> {
                if (actionHandler != null && getAdapterPosition() != RecyclerView.NO_POSITION) {
                    DownloadableSource source = downloadableSources.get(getAdapterPosition());
                    actionHandler.accept(source);
                }
            };
        }

    }

}
