package com.theoplayer.referenceapps.offline;

import android.util.Log;

import androidx.annotation.Nullable;

import com.theoplayer.android.api.cache.CachingTask;
import com.theoplayer.android.api.cache.CachingTaskStatus;

public class DownloadableSource {

    private static final String TAG = DownloadableSource.class.getSimpleName();

    private String title;
    private String poster;
    private String source;

    private boolean stateUpToDate;
    private CachingTaskStatus cachingTaskStatus;
    private double cachingTaskProgress;

    DownloadableSource() {
        this.title = "";
        this.poster = "";
        this.source = "";
        this.stateUpToDate = true;
        this.cachingTaskStatus = null;
        this.cachingTaskProgress = 0.0D;
    }

    public String getTitle() {
        return title;
    }

    public String getPoster() {
        return poster;
    }

    public String getSource() {
        return source;
    }

    public boolean isDownloadable() {
        return cachingTaskStatus != null && cachingTaskStatus != CachingTaskStatus.EVICTED;
    }

    public boolean isDownloading() {
        return cachingTaskStatus == CachingTaskStatus.LOADING;
    }

    public boolean isDownloaded() {
        return cachingTaskStatus == CachingTaskStatus.DONE;
    }

    public boolean isFailed() {
        return cachingTaskStatus == CachingTaskStatus.ERROR;
    }

    public int getProgress() {
        return (int) Math.round(cachingTaskProgress * 100);
    }

    public boolean isStateUpToDate() {
        return stateUpToDate;
    }

    public void markStateAsOutOfDate() {
        stateUpToDate = false;
    }

    /**
     * Update states of this {@code DownloadableSource} object according to corresponding state
     * of given THEOplayer's caching task.
     *
     * @param cachingTask Corresponding THEOplayer's caching task.
     * @return Returns {@code true} if state was successfully updated; {@code false} otherwise.
     */
    public boolean updateStateWith(@Nullable CachingTask cachingTask) {
        boolean stateUpdated = false;
        String logMessage = "Updated, title='" + title + "'";

        // Updating caching task status, if changed.
        CachingTaskStatus newStatus = cachingTask == null ? null : cachingTask.getStatus();
        if (cachingTaskStatus != newStatus) {
            logMessage += ", cachingTaskStatus=(" + cachingTaskStatus + " -> " + newStatus + ")";
            cachingTaskStatus = newStatus;
            stateUpdated = true;
        }

        // Updating caching task progress, if changed.
        double newProgress = cachingTask == null ? 0.0D : cachingTask.getPercentageCached();
        newProgress = cachingTaskStatus == CachingTaskStatus.DONE ? 1.0D : newProgress;
        if (cachingTaskProgress != newProgress) {
            logMessage += ", cachingTaskProgress=(" + cachingTaskProgress + " -> " + newProgress + ")";
            cachingTaskProgress = newProgress;
            stateUpdated = true;
        }

        if (stateUpdated) {
            stateUpToDate = true;
            Log.i(TAG, logMessage);
        }
        return stateUpdated;
    }

}
