# THEOplayer Android Reference Apps - THEO Offline

The purpose of this Android project is to demonstrate how to integrate THEOplayer into an Android app and playback a DASH stream.

## Getting Started on Android:

### Prerequisites

* Cloned **_THEO Offline Reference App_**.
* Android Studio  
* Obtained **_THEOplayer Android SDK_**. If you don't have a SDK yet, please request through the portal or reach out to customer success team

##### Run emulator:
- open project folder `<path to project>/android`,
- add emulator device configuration(AVD Manager).

##### Add THEOplayer SDK library:
- Please copy received THEOplayer license file theoplayer-android-[name]-[version]-minapiXX-release.aar into [libs folder](./app/libs) and rename it to theoplayer.aar.
- Sync Project with gradle files again 

Note: Please use minapi16-THEOplayer SDK for Android devices starting from 4.1 and above, while minapi21-THEOplayer SDK can be used for Android 5.0 and above.

### Build and run the app
1. Select the desire device / simulator and run the app in Android Studio. 