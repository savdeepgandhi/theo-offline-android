package com.theoplayer.referenceapps.offline;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.theoplayer.android.api.THEOplayerView;
import com.theoplayer.android.api.event.EventListener;
import com.theoplayer.android.api.event.player.EndedEvent;
import com.theoplayer.android.api.event.player.ErrorEvent;
import com.theoplayer.android.api.event.player.PauseEvent;
import com.theoplayer.android.api.event.player.PlayEvent;
import com.theoplayer.android.api.event.player.PlayerEventTypes;
import com.theoplayer.android.api.event.player.PlayingEvent;
import com.theoplayer.android.api.player.Player;
import com.theoplayer.android.api.source.SourceDescription;
import com.theoplayer.android.api.source.TypedSource;

import static com.theoplayer.android.api.source.SourceDescription.Builder.sourceDescription;
import static com.theoplayer.android.api.source.TypedSource.Builder.typedSource;

public class PlayerActivity extends AppCompatActivity {

    private static final String TAG = PlayerActivity.class.getSimpleName();

    private static final String PLAYBACK_PARAM__VIDEO_URL = "VIDEO_URL";
    private static final String PLAYBACK_PARAM__POSTER_URL = "POSTER_URL";

    private THEOplayerView theoPlayerView;
    private Player theoPlayer;

    public static void playStream(Context context, String videoUrl, String posterUrl) {
        Intent playStreamIntent = new Intent(context, PlayerActivity.class);
        playStreamIntent.putExtra(PLAYBACK_PARAM__VIDEO_URL, videoUrl);
        playStreamIntent.putExtra(PLAYBACK_PARAM__POSTER_URL, posterUrl);
        context.startActivity(playStreamIntent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        // Configuring action bar.
        setSupportActionBar(findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Gathering object references.
        theoPlayerView = findViewById(R.id.theoPlayerView);
        theoPlayer = theoPlayerView.getPlayer();

        // Configuring THEOplayer playback with parameters from intent.
        configureTHEOplayer(
                getIntent().getStringExtra(PLAYBACK_PARAM__VIDEO_URL),
                getIntent().getStringExtra(PLAYBACK_PARAM__POSTER_URL)
        );
    }

    private void configureTHEOplayer(String videoUrl, String posterUrl) {
        // Coupling the orientation of the device with the fullscreen state.
        // The player will go fullscreen when the device is rotated to landscape
        // and will also exit fullscreen when the device is rotated back to portrait.
        theoPlayerView.getSettings().setFullScreenOrientationCoupled(true);

        // Creating a TypedSource builder that defines the location of a single stream source.
        TypedSource.Builder typedSource = typedSource(videoUrl);

        // Creating a SourceDescription builder that contains the settings to be applied for a new
        // stream source.
        SourceDescription.Builder sourceDescription = sourceDescription(typedSource.build())
                .poster(posterUrl);

        // Configuring THEOplayer with defined SourceDescription object to be played automatically.
        theoPlayer.setAutoplay(true);
        theoPlayer.setSource(sourceDescription.build());

        // Adding listeners to THEOplayer basic playback events.
        theoPlayer.addEventListener(PlayerEventTypes.PLAY, onPlayEventListener);
        theoPlayer.addEventListener(PlayerEventTypes.PLAYING, onPlayingEventListener);
        theoPlayer.addEventListener(PlayerEventTypes.PAUSE, onPauseEventListener);
        theoPlayer.addEventListener(PlayerEventTypes.ENDED, onEndedEventListener);
        theoPlayer.addEventListener(PlayerEventTypes.ERROR, onErrorEventListener);
    }


    // In order to work properly and in sync with the activity lifecycle changes (e.g. device
    // is rotated, new activity is started or app is moved to background) we need to call
    // the "onResume", "onPause" and "onDestroy" methods of the THEOplayerView when the matching
    // activity methods are called.

    @Override
    protected void onPause() {
        super.onPause();
        theoPlayerView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            theoPlayerView.onResume();
        } catch (Exception exception) {
            Log.i(TAG, "", exception);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Removing THEOplayer events listeners.
        theoPlayer.removeEventListener(PlayerEventTypes.PLAY, onPlayEventListener);
        theoPlayer.removeEventListener(PlayerEventTypes.PLAYING, onPlayingEventListener);
        theoPlayer.removeEventListener(PlayerEventTypes.PAUSE, onPauseEventListener);
        theoPlayer.removeEventListener(PlayerEventTypes.ENDED, onEndedEventListener);
        theoPlayer.removeEventListener(PlayerEventTypes.ERROR, onErrorEventListener);

        theoPlayerView.onDestroy();
    }


    // THEOplayer event listeners.

    private EventListener<PlayEvent> onPlayEventListener = event ->
            Log.i(TAG, "Event: " + event.getType() + ", currentTime=" + event.getCurrentTime());

    private EventListener<PlayingEvent> onPlayingEventListener = event ->
            Log.i(TAG, "Event: " + event.getType() + ", currentTime=" + event.getCurrentTime());

    private EventListener<PauseEvent> onPauseEventListener = event ->
            Log.i(TAG, "Event: " + event.getType() + ", currentTime=" + event.getCurrentTime());

    private EventListener<EndedEvent> onEndedEventListener = event ->
            Log.i(TAG, "Event: " + event.getType() + ", currentTime=" + event.getCurrentTime());

    private EventListener<ErrorEvent> onErrorEventListener = event ->
            Log.i(TAG, "Event: " + event.getType() + ", error=" + event.getError());

}
