package com.theoplayer.referenceapps.offline;

import android.os.Bundle;
import android.text.Layout;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AlignmentSpan;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.theoplayer.android.api.THEOplayerGlobal;
import com.theoplayer.android.api.cache.Cache;
import com.theoplayer.android.api.cache.CacheStatus;
import com.theoplayer.android.api.cache.CachingParameters;
import com.theoplayer.android.api.cache.CachingTask;
import com.theoplayer.android.api.cache.CachingTaskStatus;
import com.theoplayer.android.api.event.EventListener;
import com.theoplayer.android.api.event.cache.CacheEventTypes;
import com.theoplayer.android.api.event.cache.CacheStateChangeEvent;
import com.theoplayer.android.api.event.cache.task.CachingTaskEventTypes;
import com.theoplayer.android.api.event.cache.task.CachingTaskProgressEvent;
import com.theoplayer.android.api.event.cache.task.CachingTaskStateChangeEvent;
import com.theoplayer.android.api.event.cache.tasklist.AddTaskEvent;
import com.theoplayer.android.api.event.cache.tasklist.CachingTaskListEventTypes;
import com.theoplayer.android.api.event.cache.tasklist.RemoveTaskEvent;

import java.util.Calendar;
import java.util.List;

import static com.theoplayer.android.api.source.SourceDescription.Builder.sourceDescription;


public class DownloadActivity extends AppCompatActivity {

    private static final String TAG = DownloadActivity.class.getSimpleName();

    private Cache theoCache;

    private DownloadableSourceAdapter downloadableSourceAdapter;
    private List<DownloadableSource> downloadableSources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.TheoTheme_Base);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        // Configuring action bar.
        setSupportActionBar(findViewById(R.id.toolbar));

        // Gathering object references.
        RecyclerView downloadableSourcesView = findViewById(R.id.downloadableSourcesView);
        theoCache = THEOplayerGlobal.getSharedInstance(this).getCache();
        downloadableSources = loadDownloadableSources(savedInstanceState);
        downloadableSourceAdapter = new DownloadableSourceAdapter(this, downloadableSources);

        // Configuring UI behavior and default values.
        downloadableSourceAdapter.setOnStartDownloadActionHandler(this::onStartDownload);
        downloadableSourceAdapter.setOnPauseDownloadActionHandler(this::onPauseDownload);
        downloadableSourceAdapter.setOnRemoveDownloadActionHandler(this::onRemoveDownload);
        downloadableSourceAdapter.setOnPlayActionHandler(this::onPlay);
        downloadableSourcesView.setAdapter(downloadableSourceAdapter);
        downloadableSourcesView.setLayoutManager(new LinearLayoutManager(this));

        if (theoCache != null) {
            // Defining listeners to THEOplayer cache events.
            theoCache.addEventListener(CacheEventTypes.CACHE_STATE_CHANGE, onCacheStateChangeEventListener);
            theoCache.getTasks().addEventListener(CachingTaskListEventTypes.ADD_TASK, onAddCachingTaskEventListener);
            theoCache.getTasks().addEventListener(CachingTaskListEventTypes.REMOVE_TASK, onRemoveCachingTaskEventListener);

            // If there exist any active caching task CACHE_STATE_CHANGE event will not be triggered
            // because cache is already in INITIALISED state. In such case existing caching tasks
            // needs to be recognized now, otherwise it's enough to wait for CACHE_STATE_CHANGE event.
            loadExistingCachingTasks();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Removing THEOplayer events listeners.
        if (theoCache != null) {
            theoCache.removeEventListener(CacheEventTypes.CACHE_STATE_CHANGE, onCacheStateChangeEventListener);
            theoCache.getTasks().removeEventListener(CachingTaskListEventTypes.ADD_TASK, onAddCachingTaskEventListener);
            theoCache.getTasks().removeEventListener(CachingTaskListEventTypes.REMOVE_TASK, onRemoveCachingTaskEventListener);
            for (CachingTask cachingTask : theoCache.getTasks()) {
                cachingTask.removeEventListener(CachingTaskEventTypes.CACHING_TASK_STATE_CHANGE, onCachingTaskStateChangeEventListener);
                cachingTask.removeEventListener(CachingTaskEventTypes.CACHING_TASK_PROGRESS, onCachingTaskProgressEventListener);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        // There can be situations that some caching task actions like: start, pause, remove, will
        // not be finished before user/system kills the activity. It's good to save current state so
        // it could be loaded and updated later with caching task states accordingly instead of
        // recreating it from scratch, which could be misleading.
        savedInstanceState.putString(
                getString(R.string.downloadableSourcesJsonKey),
                new Gson().toJson(downloadableSources)
        );
    }

    private List<DownloadableSource> loadDownloadableSources(@Nullable Bundle savedInstanceState) {
        String downloadableSourcesJson = null;

        if (savedInstanceState != null) {
            // Loading previously stored state of downloaded sources, if any.
            downloadableSourcesJson = savedInstanceState.getString(getString(R.string.downloadableSourcesJsonKey));
        }

        if (TextUtils.isEmpty(downloadableSourcesJson)) {
            // There was no previously stored state of downloaded sources, so creating it from
            // scratch with some defaults.
            downloadableSourcesJson = getString(R.string.downloadableSourcesJson);
        }

        // Loading downloadable sources form JSON configuration.
        return new Gson().fromJson(
                downloadableSourcesJson,
                new TypeToken<List<DownloadableSource>>() {
                }.getType()
        );
    }

    /**
     * Loads previously persisted caching tasks.
     *
     * <p>There can be cases when content is being downloaded (or is downloaded), but activity is
     * destroyed by system (or by user). After launching app again, loaded downloadable sources will
     * be updated with those caching tasks and download progress will be again presented to the user.
     */
    private void loadExistingCachingTasks() {
        if (theoCache != null && theoCache.getStatus() == CacheStatus.INITIALISED) {
            Log.i(TAG, "Found " + theoCache.getTasks().length() + " tasks...");

            for (CachingTask cachingTask : theoCache.getTasks()) {
                cachingTask.addEventListener(CachingTaskEventTypes.CACHING_TASK_STATE_CHANGE, onCachingTaskStateChangeEventListener);
                cachingTask.addEventListener(CachingTaskEventTypes.CACHING_TASK_PROGRESS, onCachingTaskProgressEventListener);
            }
            updateDownloadableSources();
        }
    }

    /**
     * Finds corresponding caching task for given downloadable source.
     *
     * <p>To have the appropriate match, source URLs needs to be the same.
     *
     * <p>Caching task can be evicted because of reaching expiration date or simply because user
     * removed it. In such cases {@code null} value is returned.
     *
     * @param downloadableSource Downloadable source for which caching task will be searched.
     * @return Found caching task or {@code null} if not exists.
     */
    @Nullable
    private CachingTask findCachingTaskFor(DownloadableSource downloadableSource) {
        if (theoCache != null) {
            for (CachingTask cachingTask : theoCache.getTasks()) {
                String source = cachingTask.getSource().getSources().get(0).getSrc();
                if (source.equals(downloadableSource.getSource())) {
                    return cachingTask;
                }
            }
        }
        return null;
    }

    /**
     * Updates downloadable sources with corresponding caching task states.
     *
     * <p>If any of them is updated then view is triggered be be repainted.
     */
    private void updateDownloadableSources() {
        boolean dataSetChanged = false;

        for (DownloadableSource downloadableSource : downloadableSources) {
            CachingTask cachingTask = findCachingTaskFor(downloadableSource);
            dataSetChanged |= downloadableSource.updateStateWith(cachingTask);
        }

        if (dataSetChanged) {
            downloadableSourceAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Outdates given {@code downloadableSource} state.
     *
     * <p>When executing any of caching task action it's worth to notify users that something
     * is being processed. Cashing task actions are executed asynchronously and we have to wait for
     * appropriate callback being called to update downloadable source state.
     *
     * @param downloadableSource Downloadable source to be outdated.
     */
    private void outdateDownloadableSource(DownloadableSource downloadableSource) {
        downloadableSource.markStateAsOutOfDate();
        downloadableSourceAdapter.notifyDataSetChanged();
    }

    private void onStartDownload(DownloadableSource downloadableSource) {
        Log.i(TAG, "Starting download task, title='" + downloadableSource.getTitle() + "'");

        if (theoCache != null) {
            // Updating UI to notify users that given downloadable source state became outdated.
            outdateDownloadableSource(downloadableSource);

            CachingTask cachingTask = findCachingTaskFor(downloadableSource);
            if (cachingTask == null) {
                CachingParameters.Builder cachingParameters = new CachingParameters.Builder();

                // By default whole content is downloaded, but here we are stating that explicitly.
                // An amount of seconds (e.g. "20") or a percentage (e.g. "50%") can be specified
                // to download only part of the content.
                cachingParameters.amount("100%");

                // By default cashing task is evicted after 30 minutes since its creation.
                // Here we want to have it expired after 2 hours since creation.
                Calendar in2Hours = Calendar.getInstance();
                in2Hours.add(Calendar.HOUR, 2);
                cachingParameters.expirationDate(in2Hours.getTime());

                // Creating caching task for given source and adding appropriate event listeners to it.
                // Newly created caching task does not start downloading automatically.
                cachingTask = theoCache.createTask(
                        sourceDescription(downloadableSource.getSource()).build(),
                        cachingParameters.build()
                );
                cachingTask.addEventListener(CachingTaskEventTypes.CACHING_TASK_STATE_CHANGE, onCachingTaskStateChangeEventListener);
                cachingTask.addEventListener(CachingTaskEventTypes.CACHING_TASK_PROGRESS, onCachingTaskProgressEventListener);
            }

            // Starting caching task, content is being downloaded.
            cachingTask.start();
        } else {
            // Being here means that caching is not supported.
            notifyUserThat(R.string.cachingNotSupported);
        }
    }

    private void onPauseDownload(DownloadableSource downloadableSource) {
        Log.i(TAG, "Pausing download task, title='" + downloadableSource.getTitle() + "'");

        CachingTask cachingTask = findCachingTaskFor(downloadableSource);
        if (cachingTask != null) {
            // Updating UI to notify users that given downloadable source state became outdated.
            outdateDownloadableSource(downloadableSource);

            // Pausing caching task, content is not being downloaded.
            cachingTask.pause();
        } else {
            // Being here means that caching is not supported or at least one of downloadable sources
            // can be outdated. Refreshing whole UI to be in sync caching task states.
            notifyUserThat(R.string.cachingTaskNotFound, downloadableSource.getTitle());
            updateDownloadableSources();
        }
    }

    private void onRemoveDownload(DownloadableSource downloadableSource) {
        Log.i(TAG, "Removing download task, title='" + downloadableSource.getTitle() + "'");

        CachingTask cachingTask = findCachingTaskFor(downloadableSource);
        if (cachingTask != null) {

            // Caching task can be removed right away or once user confirm it. That's why such
            // callback is needed.
            Runnable removeCachingTaskAction = () -> {
                // Updating UI to notify users that given downloadable source state became outdated.
                outdateDownloadableSource(downloadableSource);

                // Removing caching task, any downloaded content will be dropped.
                cachingTask.remove();
            };


            if (CachingTaskStatus.LOADING == cachingTask.getStatus()) {
                // Caching task to be removed is right now downloading content.
                // Asking user first if he/she really wants to cancel it.
                new AlertDialog.Builder(DownloadActivity.this)
                        .setTitle(downloadableSource.getTitle())
                        .setMessage(R.string.cachingTaskCancelQuestion)
                        .setPositiveButton(R.string.yes, (dialog, buttonType) -> removeCachingTaskAction.run())
                        .setNegativeButton(R.string.no, (dialog, buttonType) -> dialog.dismiss())
                        .show();

            } else {
                removeCachingTaskAction.run();
            }
        } else {
            // Being here means that caching is not supported or at least one of downloadable sources
            // can be outdated. Refreshing whole UI to be in sync caching task states.
            notifyUserThat(R.string.cachingTaskNotFound, downloadableSource.getTitle());
            updateDownloadableSources();
        }
    }

    private void onPlay(DownloadableSource downloadableSource) {
        Log.i(TAG, "Playing source, title='" + downloadableSource.getTitle() + "'");

        // There's no need to configure THEOplayer source with any caching task.
        // THEOplayer will find automatically caching task for played source if any exists.
        PlayerActivity.playStream(
                this,
                downloadableSource.getSource(),
                downloadableSource.getPoster()
        );
    }

    private void notifyUserThat(@StringRes int messageResId, Object... formatArgs) {
        // Preparing and showing Toast that displays horizontally centered message.
        SpannableString toastMessage = SpannableString.valueOf(getString(messageResId, formatArgs));
        toastMessage.setSpan(new AlignmentSpan.Standard(Layout.Alignment.ALIGN_CENTER), 0, toastMessage.length(), 0);
        Toast.makeText(this, toastMessage, Toast.LENGTH_LONG).show();
    }

    // THEOplayer event listeners.

    private EventListener<CacheStateChangeEvent> onCacheStateChangeEventListener = event -> {
        Log.i(TAG, "Event: " + event.getType() + ", state=" + theoCache.getStatus());
        loadExistingCachingTasks();
    };

    private EventListener<AddTaskEvent> onAddCachingTaskEventListener = event -> {
        Log.i(TAG, "Event: " + event.getType() + ", source=" + event.getTask().getSource().getSources().get(0).getSrc());
        updateDownloadableSources();
    };

    private EventListener<RemoveTaskEvent> onRemoveCachingTaskEventListener = event -> {
        Log.i(TAG, "Event: " + event.getType() + ", source=" + event.getTask().getSource().getSources().get(0).getSrc());
        updateDownloadableSources();
    };

    private EventListener<CachingTaskStateChangeEvent> onCachingTaskStateChangeEventListener = event -> {
        Log.i(TAG, "Event: " + event.getType());
        updateDownloadableSources();
    };

    private EventListener<CachingTaskProgressEvent> onCachingTaskProgressEventListener = event -> {
        Log.i(TAG, "Event: " + event.getType());
        updateDownloadableSources();
    };

}
